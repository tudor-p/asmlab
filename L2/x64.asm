;x86-64 assembly file for ML64 assembler (tested in VS2015)
EXTERN ExitProcess:PROC ;external procedure to be called upon application exit to give control back to the OS

_DATA SEGMENT	;data segment symbols
_DATA ENDS

_TEXT SEGMENT	;code segment
main proc	;app entry point (name "main" is essential)

	mov	rax,5	;your code (try not to mess up with SP and BP registers...)
	add	rax,6

	call ExitProcess	;gracefully return control to the operating system!
main endp ;end of entry point procedure
_TEXT ENDS

END
