# Programare în limbaj de asamblare

## Materiale suport pentru laboratoare

Materialele suport spcefice fiecărui laborator sunt structurate în directoarele L1, L2, ...

## Resurse online utile

### Compilator online

Platforma [godbolt.org](https://godbolt.org) pune la dispoziție un set de comilatoare pentru diverse limbaje (C, C++, D, Go, Pascal, Haskel) care produc cod în limbaj de asamblare pentru diverse mașini (x86, x86-64, ARM, ARM64).

Codul generat este interactiv: fiecare instrucțiune este explicată dacă se staționează cu mouseul deasupra ei.

Deasemenea este folosit un cod de culori care ușurează urmărirea corespondenței între structurile de control ale limbajului de nivel înalt cu instrucțiunile în limbaj de asamblare.

### x86-64

Introducere în limbajul de asamblare x86-64 de la Intel [Introduction_to_x64_Assembly.pdf](https://software.intel.com/sites/default/files/m/d/4/1/d/8/Introduction_to_x64_Assembly.pdf).